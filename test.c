#include <stdio.h>

/*This is a simple while loop script */

int main()
{
	int x = 0;
	while(x <= 100)
	{
		printf("Git is awesome\n");
		x++;
	}
	return 0;
}
